﻿using System;

namespace UnderTheBrand.Infrastructure.DTO.Base
{
    public class EntityDTO
    {
        public Guid Id { get; set; }
    }
}
